package com.information.info;

import com.information.info.controller.admin.InfoController;
import com.information.info.controller.front.InfoFrontController;
import com.information.info.entity.Info;
import com.information.info.repository.InfoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InfoApplicationTests {
	@Autowired
	private InfoController infoController;
	@Autowired
	private InfoRepository infoRepository;
	@Autowired
	private InfoFrontController infoFrontController;
	@Test
	public void contextLoads() {
		List<Info> infoList= (List<Info>) infoRepository.findAll();
		for(Info s:infoList){
			System.out.println(s);
		}
	}
	@Test
	public void testfindall(){
		Iterable<Info> infoIterable=infoFrontController.online_infos();
		for(Info i:infoIterable){
			System.out.println(i);
		}
	}
	@Test
	public void testfindid(){
		Info info=infoFrontController.online_info(new Long(1));
		System.out.println(info);
	}
	@Test
	public void testadd(){
		Info info=Info.builder()
				.title("spring")
				.name("curry")
				.date("1993-11-12")
				.content("NBA Player")
				.album("/temp/stephen")
				.flag(1)
				.build();
		infoController.add(info);
		Info temp=infoFrontController.online_info(new Long(4));
		System.out.println(temp);
	}
	@Test
	public void testinfosBypage(){
		Iterable<Info> infoIterable=infoController.infos(0,2,1);
		for(Info i:infoIterable){
			System.out.println(i);
		}
	}
	@Test
	public void testup(){
		Info info=infoController.up(new Long(5));
		System.out.println(info);
	}
	@Test
	public void testdown(){
		Info info=infoController.down(new Long(5));
		System.out.println(info);
	}
	@Test
	public void testsearch(){
		Iterable<Info> infoIterable=infoController.search("world");
		for(Info i:infoIterable){
			System.out.println(i);
		}
	}
}
