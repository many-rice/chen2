package com.information.info.controller.vo;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.List;

@Data
@Builder
public class AuthorityVo {
    private Long id;
    private String phone;
    private String email;
    private String realName;
    private List<Role> authorities;
    private String address;

    @Tolerate
    public AuthorityVo() {
    }

    @Data
    @Builder
    public static class Role {
        private String authority;

        @Tolerate
        public Role() {
        }
    }
}